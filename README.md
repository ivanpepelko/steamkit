#Installation
Install with composer: `composer require ivanpepelko/steamkit`

Setup your config file(see [Config](#markdown-header-config) section) and run: `vendor/bin/steamkit generate-api`

#Config
Create `config.yml` file in root directory of your project or pass `--config` parameter to console utility to read from custom config file.
Only yaml/yml format is supported currently.

##Sample Config
```yml
#config resides in steamkit section
steamkit:
    # your Steam Web API key
    apikey: ~ 
    # response from steam api is associative array or objects
    assoc: true
```

#Console utility

To generate Steam Web API classes: `php vendor/bin/steamkit generate-api`

##Parameters
```
Short   Long        Description
-c      --config    Custom config file. Default config file is config.yml in the root directory of project.
-f      --fix       Run php-cs-fixer on generated files after generation.
```
Example for Symfony 3 users (if values are provided in default config file):
    `php vendor/bin/generate-api generate --config app/config/config.yml`
