<?php

/*
 * The MIT License
 *
 * Copyright 2016 Ivan Pepelko.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace SteamKit\Console\Command;

use GuzzleHttp\Client;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\Yaml\Yaml;
use Symfony\CS\Console\Command\FixCommand;
use function GuzzleHttp\json_decode;

/**
 * Description of GenerateCommand
 *
 * @author ivanp
 */
class GenerateCommand extends Command {

    const STRING_CONFIG_NOT_FOUND = 0;
    const STRING_FAULTY_CONFIG = 1;
    const STRING_CONFIG_NO_APIKEY = 2;
    const STRING_ERROR_DATA_FETCH = 3;
    const STRING_LICENSE = 4;

    private $apikey;
    private $assoc;

    public function __construct() {
        parent::__construct();
    }

    protected function configure() {
        $this->setName('generate-api')
                ->setAliases(['genapi'])
                ->setDescription('Generates Steam Web API classes.')
                ->addOption('config', 'c', InputOption::VALUE_REQUIRED, 'Custom config file. Default config file is config.yml.');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $configOption = $input->getOption('config');
        if ('=' === $configOption[0]) {
            $configOption = substr($configOption, 1);
        }

        $directory = getcwd() . (file_exists(getcwd() . '/src/SteamKit') ? '' : '/vendor/ivanpepelko/steamkit');
        $configFilename = realpath($configOption ? getcwd() . DIRECTORY_SEPARATOR . $configOption : './config.yml');
        if (!file_exists($configFilename)) {
            $output->writeln('<error>' . $this->getString(self::STRING_CONFIG_NOT_FOUND) . '</error>');
            exit(1);
        }
        file_put_contents($directory . '/config_path.txt', $configFilename);
        if ($output->isVerbose()) {
            $output->writeln('Reading config file: ' . $configFilename);
        }
        $config = Yaml::parse(file_get_contents($configFilename));
        if (!array_key_exists('steamkit', $config)) {
            $output->writeln('<error>}' . $this->getString(self::STRING_FAULTY_CONFIG) . '</error>');
            exit(1);
        }
        if (!array_key_exists('apikey', $config['steamkit']) || null == $config['steamkit']['apikey']) {
            $output->writeln('<error>' . $this->getString(self::STRING_CONFIG_NO_APIKEY) . '</error>');
            exit(1);
        }
        $this->apikey = $config['steamkit']['apikey'];
        $this->assoc = array_key_exists('object_response', $config['steamkit']) ? $config['steamkit']['object_response'] : false;
        if ($output->isVerbose()) {
            $output->writeln('Fetching data from Steam API.');
        }
        $client = new Client();
        $apiList = $client->get("http://api.steampowered.com/ISteamWebAPIUtil/GetSupportedAPIList/v1/", [
            'query' => [
                'key' => $this->apikey
            ]
        ]);
        if (200 !== $apiList->getStatusCode()) {
            $output->writeln('<error>' . $this->getString(self::STRING_ERROR_DATA_FETCH) . '</error>');
            exit(1);
        }
        $api = json_decode($apiList->getBody());
        if ($output->isVerbose()) {
            $output->writeln('Data fetched.');
        }
        if (!file_exists($directory . '/src/SteamKit/WebApi')) {
            mkdir($directory . '/src/SteamKit/WebApi');
        }
        if ($output->isVerbose()) {
            $output->writeln('Writing classes.');
        }
        foreach ($api->apilist->interfaces as $interface) {
            if ($output->isVeryVerbose()) {
                $className = substr($interface->name, 1);
                $output->writeln("<info>Writing class:</info> $className");
            }
            file_put_contents($directory . '/src/SteamKit/WebApi/' . substr($interface->name, 1) . '.php', $this->generateClassBody($interface));
        }
        $output->writeln('<info>Generation complete!</info>');
    }

    private function generateClassBody($interface) {
        $className = substr($interface->name, 1);
        $license = $this->getString(self::STRING_LICENSE);
        $phpNewUseSyntax = version_compare(PHP_VERSION, '5.6', '>=') ? "\nuse function GuzzleHttp\json_decode;" : '';
        $classHead = <<<EOL
<?php

$license

namespace SteamKit\WebApi;

use GuzzleHttp\Client;$phpNewUseSyntax

class $className extends AbstractWebApi {


EOL;
        $methods = '';
        foreach ($interface->methods as $method) {
            $methods .= $this->generateMethodBody($method, $interface->name);
        }
        $classEnd = "}";
        return $classHead . $methods . $classEnd;
    }

    private function generateMethodBody($method, $ifName) {
        $mName = lcfirst($method->name);
        $queryParamsArray = [
            "'key' => \$this->apikey"
        ];
        foreach ($method->parameters as $parameter) {
            if ('key' === $parameter->name)
                continue;
            $queryParamsArray[] = "'{$parameter->name}' => \${$parameter->name}";
        }
        $queryParamsString = '[' . implode(', ', $queryParamsArray) . ']';
        $assocString = $this->assoc ? 'true' : 'false';
        $mbody = <<<EOL
    {$this->generateMethodDocs($method)}
    public function {$mName}v{$method->version}({$this->generateMethodParameters($method->parameters)}) {
        \$queryParams = $queryParamsString;
        \$client = new Client();
        \$response = \$client->request('{$method->httpmethod}', 'http://api.steampowered.com/$ifName/{$method->name}/v{$method->version}/', [

EOL;
        switch (strtolower($method->httpmethod)) {
            case 'get':
                $mbody .= "'query' => \$queryParams";
                break;
            case 'post':
                $mbody .= <<<EOL
            'body' => \GuzzleHttp\Psr7\build_query(\$queryParams),
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded'
            ]
EOL;
                break;
        }
        $mbody .= <<<EOL

        ]);
        return json_decode(\$response->getBody(), $assocString);

EOL;

        $mbody .= "\n}\n";
        return $mbody;
    }

    private function generateMethodDocs($method) {
        $start = "/**\n";
        $description = ' * ' . (property_exists($method, 'description') ? $method->description : $method->name) . "\n";
        $paramList = [];
        foreach ($method->parameters as $parameter) {
            if ('key' === $parameter->name)
                continue;
            $paramList[] = " * @param "
                    . $this->fixParameterType($parameter->type)
                    . ' $'
                    . $this->fixParameterName($parameter->name)
                    . ' '
                    . (property_exists($parameter, 'description') ? $parameter->description : '');
        }
        $end = "\n */";
        return $start . $description . implode("\n", $paramList) . $end;
    }

    private function generateMethodParameters($parameters) {
        if (0 == count($parameters))
            return "";

        $paramList = [];
        foreach ($parameters as $parameter) {
            $pName = $this->fixParameterName($parameter->name);
            if ('key' === $pName)
                continue;
            $paramList[] = '$' . $pName . ($parameter->optional ? ' = null' : '');
        }
        return implode(', ', $paramList);
    }

    private function fixParameterName($name) {
        return lcfirst(preg_replace('/\[\d+\]/', '', $name));
    }

    private function fixParameterType($type) {
        $types = [
            "uint32" => "int",
            "uint64" => "int",
            "string" => "string",
            "int32" => "int",
            "bool" => "bool",
            "rawbinary" => "string",
            "{message}" => "string"
        ];

        return $types[$type];
    }

    private function getString($str) {
        switch (strtolower($str)) {
            case self::STRING_CONFIG_NOT_FOUND:
                return 'Config file not found! Create config.yml file in the root directory of your project or specify custom config file with --config parameter.';
            case self::STRING_FAULTY_CONFIG:
                return 'Config file does not contain \'steamkit\' section.';
            case self::STRING_CONFIG_NO_APIKEY:
                return 'Steam API key is not defined in config file.';
            case self::STRING_ERROR_DATA_FETCH:
                return 'Error fetching data from Steam API.';
            case self::STRING_LICENSE:
                return <<<EOL
/*
 * The MIT License
 *
 * Copyright 2016 Ivan Pepelko.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
EOL;
            default:
                return '';
        }
    }

}
