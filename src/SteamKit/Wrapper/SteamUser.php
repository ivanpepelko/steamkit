<?php

/*
 * The MIT License
 *
 * Copyright 2016 Ivan Pepelko.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace SteamKit\Wrapper;

use DateTime;
use DateTimeZone;
use SteamKit\WebApi\SteamUser as SteamApiUser;

/**
 * Description of SteamUserWrapper
 *
 * @author ivanp
 */
class SteamUser {

    /**
     * @var SteamID
     */
    private $steamid;

    /**
     * @var type
     */
    private $community_visibility_state;
    private $profile_state;
    private $persona_name;

    /**
     *
     * @var DateTime
     */
    private $last_logoff;
    private $comment_permission;
    private $profile_url;
    private $avatar;
    private $avatar_medium;
    private $avatar_full;
    private $persona_state;

    /**
     * private information
     */
    private $primary_clan_id;
    private $time_created;
    private $persona_state_flags;
    private $loc_country_code;
    private $loc_state_code;
    private $loc_city_id;

    public function __construct($steamid) {
        if (!$steamid instanceof SteamID) {
            $steamid = new SteamID($steamid);
        }
        $this->steamid = $steamid;
        $userService = new SteamApiUser();
        $steamData = $userService->getPlayerSummariesv2($steamid->getCommunityID());

        $profileData = $steamData->response->players[0];

        $this->community_visibility_state = $profileData->communityvisibilitystate;
        $this->profile_state = $profileData->profilestate;
        $this->persona_name = $profileData->personaname;
        $this->last_logoff = DateTime::createFromFormat('U', $profileData->lastlogoff);
        if (ini_get('date.timezone'))
            $this->last_logoff->setTimezone(new DateTimeZone(ini_get('date.timezone')));
        $this->profile_url = $profileData->profileurl;
        $this->avatar = $profileData->avatar;
        $this->avatar_medium = $profileData->avatarmedium;
        $this->avatar_full = $profileData->avatarfull;
        $this->persona_state = $profileData->personastate;

        if (property_exists($profileData, 'commentpermission'))
            $this->comment_permission = $profileData->commentpermission;

        if (property_exists($profileData, "primaryclanid"))
            $this->primary_clan_id = $profileData->primaryclanid;

        if (property_exists($profileData, "timecreated")) {
            $this->time_created = DateTime::createFromFormat('U', $profileData->timecreated);
            if (ini_get('date.timezone'))
                $this->time_created->setTimezone(new DateTimeZone(ini_get('date.timezone')));
        }

        if (property_exists($profileData, "personastateflags"))
            $this->persona_state_flags = $profileData->personastateflags;

        if (property_exists($profileData, "loccountrycode"))
            $this->loc_country_code = $profileData->loccountrycode;

        if (property_exists($profileData, "locstatecode"))
            $this->loc_state_code = $profileData->locstatecode;
        if (property_exists($profileData, "loccityid"))
            $this->loc_city_id = $profileData->loccityid;
    }

    public function getSteamID() {
        return $this->steamid;
    }

    public function getCommunityVisibilityState() {
        return $this->community_visibility_state;
    }

    public function getProfileState() {
        return $this->profile_state;
    }

    public function getPersonaName() {
        return $this->persona_name;
    }

    public function getLastLogoff() {
        return $this->last_logoff;
    }

    public function getCommentPermission() {
        return $this->comment_permission;
    }

    public function getProfileUrl() {
        return $this->profile_url;
    }

    public function getAvatar() {
        return $this->avatar;
    }

    public function getAvatarMedium() {
        return $this->avatar_medium;
    }

    public function getAvatarFull() {
        return $this->avatar_full;
    }

    public function getPersonaState() {
        return $this->persona_state;
    }

    public function getPrimaryClanId() {
        return $this->primary_clan_id;
    }

    public function getTimeCreated() {
        return $this->time_created;
    }

    public function getPersonaStateFlags() {
        return $this->persona_state_flags;
    }

    public function getLocCountryCode() {
        return $this->loc_country_code;
    }

    public function getLocStateCode() {
        return $this->loc_state_code;
    }

    public function getLocCityId() {
        return $this->loc_city_id;
    }

}
