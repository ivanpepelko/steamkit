<?php

/*
 * The MIT License
 *
 * Copyright 2016 Ivan Pepelko.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace SteamKit\Wrapper;

use Exception;
use SteamKit\WebApi\SteamUser as SteamApiUser;

/**
 * Description of SteamID
 *
 * @author ivanp
 */
class SteamID extends AbstractSteamID {

    public function __construct($input) {
        $input = trim($input);

        $legacyIDRegex = '(^STEAM_([0-4]):([01]):(\d+)$)';
        $modernIDRegex = '(^\[?([IUMGAPCgTcLa]):([0-4]):(\d+)(:\d+)?\]?$)';
        $customInputRegex = '(([A-Za-z0-9-_]+)\/?$)';
        $matches = [];
        preg_match("/$customInputRegex|$modernIDRegex|$legacyIDRegex/", $input, $matches);

        if (!$matches) {
            throw new Exception('Unsupported input.');
        }

        if ($matches[1] && $matches[2]) {
            if (is_numeric($matches[2])) {
                $this->communityID = self::fixNumericId($matches[2]);
            } else {
                $this->customURL = $matches[2];
                $userService = new SteamApiUser();
                $resolvedId = $userService->resolveVanityURLv1($matches[2]);
                $this->communityID = $resolvedId->response->steamid;
            }
            if ($this->communityID == '76561197960265728') {
                $this->setAccountType(self::TYPE_ANONUSER);
            }
            $binary = str_pad(decbin($this->communityID), 64, '0', STR_PAD_LEFT);
            $this->universe = bindec(substr($binary, 0, 8));
            $this->setAccountType(bindec(substr($binary, 8, 4)));
            $this->accountID = bindec(substr($binary, 32));
            $this->legacyID = 'STEAM_0:' . $binary[63] . ':' . bindec(substr($binary, 32, 31));
            $this->modernID = '[' . self::accTypeIDToChar($this->getAccountType()) . ':' . $this->universe . ':' . $this->accountID . ']';
        } elseif ($matches[3] && $matches[4] && !is_null($matches[5]) && $matches[6]) {
            $this->modernID = $matches[3];
            $this->accountID = $matches[6];
            $obj = new static($this->accountID);
            $this->setAccountType($obj->getAccountType());
            $this->universe = $obj->getUniverse();
            $this->legacyID = $obj->getLegacyID();
            unset($obj);
            $this->communityID = self::fixNumericId($this->accountID);
        } elseif ($matches[8] && !is_null($matches[9]) && !is_null($matches[10]) && $matches[11]) {
            //$this->legacyID = $matches[8];
            $binary = str_pad(decbin($matches[11]), 31, '0', STR_PAD_LEFT);
            $this->accountID = bindec($binary . $matches[10]);
            $this->communityID = self::fixNumericId($this->accountID);
            $obj = new static($this->communityID);
            $this->legacyID = $obj->getLegacyID();
            $this->universe = $obj->getUniverse();
            $this->modernID = $obj->getModernID();
            $this->setAccountType($obj->getAccountType());
            unset($obj);
        }
    }

    private static function fixNumericId($id) {
        $idComp = bccomp($id, '76561197960265728');
        if ($idComp == 1) {
            $steamId = $id;
        } elseif ($idComp == -1) {
            $steamId = bcadd($id, '76561197960265728');
        } else {
            return '76561197960265728';
        }
        return $steamId;
    }

    private static function accTypeIDToChar($id) {
        $types = [
            0 => 'I',
            1 => 'U',
            2 => 'M',
            3 => 'G',
            4 => 'A',
            5 => 'P',
            6 => 'C',
            7 => 'g',
            8 => 'T',
            8 => 'c',
            8 => 'L',
            10 => 'a'
        ];
        return $types[$id];
    }

}
