<?php

/*
 * The MIT License
 *
 * Copyright 2016 Ivan Pepelko.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace SteamKit\Wrapper;

/**
 *
 * @author ivanp
 */
interface SteamIDInterface {

    /**
     * Account types
     */
    const TYPE_INVALID = 0;
    const TYPE_INDIVIDUAL = 1;
    const TYPE_MULTISEAT = 2;
    const TYPE_GAMESERVER = 3;
    const TYPE_ANONGAMESERVER = 4;
    const TYPE_PENDING = 5;
    const TYPE_CONTENTSERVER = 6;
    const TYPE_CLAN = 7;
    const TYPE_CHAT = 8;
    const TYPE_CHATCLAN = 8;
    const TYPE_CHATLOBBY = 8;
    const TYPE_P2PSUPERSEEDER = 9;
    const TYPE_ANONUSER = 10;

    /**
     * Universes
     */
    const UNIVERSE_INVALID = 0;
    const UNIVERSE_PUBLIC = 1;
    const UNIVERSE_BETA = 2;
    const UNIVERSE_INTERNAL = 3;
    const UNIVERSE_DEV = 4;

    public function getLegacyID();

    public function getModernID();

    public function getAccountID();

    public function getCommunityID();

    public function getCustomURL();

    public function getUniverse();

    public function getAccountType();
}
