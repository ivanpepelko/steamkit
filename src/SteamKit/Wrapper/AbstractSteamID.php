<?php

/*
 * The MIT License
 *
 * Copyright 2016 Ivan Pepelko.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace SteamKit\Wrapper;

/**
 * Description of AbstractSteamID
 *
 * @author ivanp
 */
abstract class AbstractSteamID implements SteamIDInterface {

    protected $legacyID;
    protected $modernID;
    protected $accountID;
    protected $communityID;
    protected $customURL;
    protected $universe;
    private $accountType;

    public function getLegacyID() {
        return $this->legacyID;
    }

    public function getModernID() {
        return $this->modernID;
    }

    public function getCommunityID() {
        return $this->communityID;
    }

    public function getCustomURL() {
        return $this->customURL;
    }

    protected function setAccountType($value) {
        if (is_numeric($value)) {
            $this->accountType = (int) $value;
        } else {
            switch ($value) {
                case 'I':
                    $this->accountType = self::TYPE_INVALID;
                    break;
                case 'U':
                    $this->accountType = self::TYPE_INDIVIDUAL;
                    break;
                case 'M':
                    $this->accountType = self::TYPE_MULTISEAT;
                    break;
                case 'G':
                    $this->accountType = self::TYPE_GAMESERVER;
                    break;
                case 'A':
                    $this->accountType = self::TYPE_ANONGAMESERVER;
                    break;
                case 'P':
                    $this->accountType = self::TYPE_PENDING;
                    break;
                case 'C':
                    $this->accountType = self::TYPE_CONTENTSERVER;
                    break;
                case 'g':
                    $this->accountType = self::TYPE_CLAN;
                    break;
                case 'T':
                case 'c':
                case 'L':
                    $this->accountType = self::TYPE_CHAT;
                    break;
                case '':
                    $this->accountType = self::TYPE_P2PSUPERSEEDER;
                    break;
                case 'a':
                default:
                    $this->accountType = self::TYPE_ANONUSER;
                    break;
            }
        }
    }

    public function getUniverse() {
        return $this->universe;
    }

    public function getAccountType() {
        return $this->accountType;
    }

    public function getAccountID() {
        return $this->accountID;
    }

    public function hasCustomURL() {
        return (bool) $this->customURL;
    }

    public function isAnonymous() {
        return $this->accountType == self::TYPE_ANONUSER;
    }

    public function __toString() {
        return strval($this->communityID);
    }

}
